package com.testinsta.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testinsta.R
import com.testinsta.model.Currency
import kotlinx.android.synthetic.main.item_currency.view.*

class CurrencyAdapter: RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {
    private val list = mutableListOf<Currency>()

    fun setItems(currencyList: List<Currency>) {
        list.clear()
        list.addAll(currencyList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_currency, parent, false)
        return CurrencyViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(list[position])
    }

    class CurrencyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val textName = itemView.textName
        private val textPrice = itemView.textPrice
        fun bind(currency: Currency) {
            textName.text = currency.pair
            textPrice.text = currency.price.toString()
        }
    }
}