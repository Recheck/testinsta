package com.testinsta.currency

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.testinsta.R
import com.testinsta.adapter.CurrencyAdapter
import kotlinx.android.synthetic.main.activity_currency_list.*
import org.koin.android.ext.android.inject

class CurrencyActivity : AppCompatActivity() {

    companion object {
        const val TOKEN = "token"
        const val LOGIN = "login"

        fun getInstance(context: Context, token: String, login: String) {
            val intent = Intent(context, CurrencyActivity::class.java)
            intent.putExtra(TOKEN, token)
            intent.putExtra(LOGIN, login)
            context.startActivity(intent)
        }
    }

    private val currencyViewModel: CurrencyViewModel by inject()
    private val currencyAdapter: CurrencyAdapter by lazy {
        CurrencyAdapter()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_list)
        recyclerView.adapter = currencyAdapter
        swipe.setOnRefreshListener {
            updateData()
            swipe.isRefreshing = false
        }

        updateData()

        currencyViewModel.liveCurrencyList.observe(this, Observer {
            currencyAdapter.setItems(it)
        })
    }

    fun updateData(){
        intent?.let {
            currencyViewModel.getCurrencyList(intent.getStringExtra(LOGIN), intent.getStringExtra(TOKEN))
        }
    }
}