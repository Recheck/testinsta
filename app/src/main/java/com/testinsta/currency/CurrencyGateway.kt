package com.testinsta.currency

import android.util.Log
import com.testinsta.model.Currency
import com.testinsta.network.Api
import io.reactivex.rxjava3.core.Single

class CurrencyGateway(private val api: Api) {

    fun getCurrencyList(login: String, pairs: String, token: String) : Single<List<Currency>> {
        return api.getCurrencyList(token, login, pairs, System.currentTimeMillis() - 604800, System.currentTimeMillis())
    }

}