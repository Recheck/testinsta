package com.testinsta.currency

import com.testinsta.model.Currency
import io.reactivex.rxjava3.core.Single

class CurrencyInteractor(private val currencyGateway: CurrencyGateway) {
    private val pairs = "EURUSD,GBPUSD,USDJPY,USDCHF,USDCAD,AUDUSD,NZDUSD"

    fun getCurrencyList(login: String, token: String) : Single<List<Currency>> {
        return currencyGateway.getCurrencyList(login, pairs, token)
            .map { it.sortedByDescending { it.time } }
    }
}