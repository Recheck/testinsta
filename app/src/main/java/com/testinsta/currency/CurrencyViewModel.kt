package com.testinsta.currency

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.testinsta.model.Currency
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject

class CurrencyViewModel : ViewModel(), KoinComponent {
    private val currencyInteractor: CurrencyInteractor by inject()
    private val compositeDisposable = CompositeDisposable()
    val liveCurrencyList = MutableLiveData<List<Currency>>()

    fun getCurrencyList(login: String, token: String) {
        currencyInteractor.getCurrencyList(login, token)
            .doOnSubscribe { compositeDisposable.add(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                liveCurrencyList.value = it
            }, {
                Log.d("myLogs", "Error = $it")
            })
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}