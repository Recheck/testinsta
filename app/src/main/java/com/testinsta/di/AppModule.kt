package com.testinsta.di

import com.testinsta.currency.CurrencyGateway
import com.testinsta.currency.CurrencyInteractor
import com.testinsta.currency.CurrencyViewModel
import com.testinsta.main.AuthGateway
import com.testinsta.main.MainInteractor
import com.testinsta.main.MainViewModel
import com.testinsta.network.Api
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val myModule : Module = module {
    viewModel { MainViewModel() }
    single { MainInteractor(get()) }
    single { AuthGateway(get()) }

    viewModel { CurrencyViewModel() }
    single { CurrencyInteractor(get()) }
    single { CurrencyGateway(get()) }
}

val networkModule = module {
    factory { provideApi(get()) }
    single { provideRetrofit() }
}

fun provideApi(retrofit: Retrofit) : Api =
    retrofit.create(Api::class.java)

fun provideRetrofit(): Retrofit {
    val okHttpClient = OkHttpClient().newBuilder().build()
    return Retrofit.Builder().baseUrl("http://client-api.instaforex.com/")
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}