package com.testinsta.main

import com.testinsta.model.AuthorizationModel
import com.testinsta.network.Api
import io.reactivex.rxjava3.core.Single

class AuthGateway(val api: Api) {

    fun login(authorizationModel: AuthorizationModel): Single<Pair<String, String>> {
        return api.login(authorizationModel)
            .map { it to authorizationModel.login }
    }
}