package com.testinsta.main

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.testinsta.R
import com.testinsta.currency.CurrencyActivity
import com.testinsta.model.AuthorizationModel
import com.testinsta.model.ERROR
import com.testinsta.model.SUCCESS
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel.navToCurrency.observe(this, Observer {
            if (it.status == SUCCESS) {
                it.pair?.let { CurrencyActivity.getInstance(this, it.first, it.second) }
            } else if (it.status == ERROR) {
                showError()
            }
        })

        buttonSignIn.setOnClickListener {
            mainViewModel.login(
                AuthorizationModel(
                    editLogin.text.toString(),
                    editPassword.text.toString()
                )
            )
        }
    }

    private fun showError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.error_login)
            .setMessage(R.string.error_login_message)
            .setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }
}
