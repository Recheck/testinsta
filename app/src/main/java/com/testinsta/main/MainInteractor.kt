package com.testinsta.main

import com.testinsta.model.AuthorizationModel
import io.reactivex.rxjava3.core.Single
import java.lang.IllegalArgumentException

class MainInteractor(private val authGateway: AuthGateway){

    fun login(authorizationModel: AuthorizationModel) : Single<Pair<String, String>> {
        val username = authorizationModel.login
        val password = authorizationModel.password
        if (username.isNotEmpty() && password.isNotEmpty()) {
            return authGateway.login(authorizationModel)
        }
        return Single.error(IllegalArgumentException())
    }
}