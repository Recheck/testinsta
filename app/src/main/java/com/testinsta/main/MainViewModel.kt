package com.testinsta.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.testinsta.model.AuthorizationModel
import com.testinsta.model.ERROR
import com.testinsta.model.LoginResponse
import com.testinsta.model.SUCCESS
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject

class MainViewModel : ViewModel(), KoinComponent {
    private val compositeDisposable = CompositeDisposable()
    private val mainInteractor: MainInteractor by inject()
    val navToCurrency = MutableLiveData<LoginResponse>()
    private var waitingForResponse: Boolean = false

    fun login(authorizationModel: AuthorizationModel) {
        if (waitingForResponse) return
        waitingForResponse = true
        mainInteractor.login(authorizationModel)
            .doOnSubscribe { compositeDisposable.add(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                navToCurrency.value = LoginResponse(SUCCESS, it)
                waitingForResponse = false
            }, {
                Log.d("myLogs", "Error = $it")
                navToCurrency.value = LoginResponse(ERROR)
                waitingForResponse = false
            })
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}