package com.testinsta.model

data class AuthorizationModel(val login: String, val password: String)