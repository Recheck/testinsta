package com.testinsta.model

import com.google.gson.annotations.SerializedName

data class Currency(
    @SerializedName("ActualTime")
    val time: Long,
    @SerializedName("Pair")
    val pair: String,
    @SerializedName("Price")
    val price: Double
) {

}