package com.testinsta.model

class LoginResponse(val status: Int, val pair: Pair<String, String>? = null)

const val SUCCESS = 0
const val ERROR = 1