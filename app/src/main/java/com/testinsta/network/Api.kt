package com.testinsta.network

import com.google.gson.JsonElement
import com.testinsta.model.AuthorizationModel
import com.testinsta.model.Currency
import io.reactivex.rxjava3.core.Single
import retrofit2.http.*

interface Api {
    @POST("api/Authentication/RequestMoblieCabinetApiToken")
    fun login(@Body authorizationModel: AuthorizationModel) : Single<String>

    @GET("clientmobile/GetAnalyticSignals/{login}?tradingsystem=3&from=1587408416&to=1587926816")
    fun getCurrencyList(
        @Header("passkey") token: String,
        @Path("login") login: String,
        @Query("pairs") pairs: String,
        @Query("from") from: Long,
        @Query("to") to: Long
    ) : Single<List<Currency>>
}